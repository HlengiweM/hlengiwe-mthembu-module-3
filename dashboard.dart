import 'dart:ui';

import 'package:flutter/material.dart';
import './profile_edit.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Dashboard'),
      ),
      body: Center(
        child: Container(
          color: Colors.blueAccent,
          height: 300,
          width: 200,
          padding: EdgeInsets.all(0.8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(150, 50), primary: Colors.blue),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => hiking()));
                  },
                  label: Text('Hiking'),
                  icon: Icon(Icons.emoji_people_sharp)),
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                    fixedSize: Size(150, 50), primary: Colors.blue),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => last_feature()));
                },
                label: Text('Engagement Group'),
                icon: Icon(Icons.chat),
              ),
              ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                    fixedSize: Size(150, 50), primary: Colors.blue),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profile()));
                },
                label: Text('Profile edit'),
                icon: Icon(Icons.face),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.popUntil(
            context,
            ModalRoute.withName(Navigator.defaultRouteName),
          );
        },
        child: Icon(Icons.logout_rounded),
      ),
    );
  }
}

class hiking extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Hiking Events'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              color: Color.fromARGB(255, 0, 3, 8),
              child: Text(
                'Welcome to the Upcoming hiking events viewing!',
                style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 100.0),
          Center(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    /*decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/henops.jpg'),
                          fit: BoxFit.contain),
                    ),
                    
                  */
                    child: Image.asset(
                      'assets/henops.jpg',
                      height: 300,
                      width: 150,
                    ),
                  ),
                  Column(
                    children: [
                      Text(
                        'Henops hiking trail',
                        style:
                            TextStyle(color: Colors.redAccent, fontSize: 22.0),
                      ),
                      Text(
                        '- Event date : 21 March 2020',
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                      Text(
                        '- Event Time : 09h00',
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                      Text(
                        '- tickets : ww.hikingtraick.vt',
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class last_feature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Hiking Events'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 350,
              height: 400,
              color: Colors.blueGrey,
              child: Text(
                'This is the last feature screen',
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
