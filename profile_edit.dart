import 'package:flutter/material.dart';

class profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Edit Profile'),
      ),
      body: Column(
        children: [
          Center(
            child: CircleAvatar(
              radius: 30.0,
              backgroundColor: Color.fromARGB(255, 33, 42, 49),
              child: Icon(Icons.face, size: 50.0, color: Colors.white10),
            ),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'Full Name',
                hintText: 'Hlengiwe Mthembu',
                border: OutlineInputBorder()),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'phone',
                hintText: '07333542311',
                border: OutlineInputBorder()),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'password',
                hintText: '*******',
                border: OutlineInputBorder()),
          ),
          TextField(
            obscureText: true,
            decoration: InputDecoration(
                labelText: 're-type password',
                hintText: '*******',
                border: OutlineInputBorder()),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ButtonTheme(
                minWidth: 80.0,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Save'),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
