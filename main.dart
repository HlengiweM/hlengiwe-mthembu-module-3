// ignore_for_file: avoid_renaming_method_parameters, deprecated_member_use

import 'package:flutter/material.dart';
import './dashboard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: loginPage(),
    );
  }
}

class loginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Login'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            CircleAvatar(
              radius: 30.0,
              backgroundColor: Colors.blue,
              child: Icon(Icons.face, size: 50.0, color: Colors.white10),
            ),
            Text(
              'Welcome',
              style: TextStyle(fontSize: 32.0, color: Colors.blue),
            ),
            Container(
              color: Colors.redAccent,
              child: Row(
                children: [
                  Icon(Icons.phone),
                  SizedBox(child: Text('Enter Phone number')),
                ],
              ),
            ),
            Container(
              width: 30,
              height: 30,
              color: Colors.white,
            ),
            Container(
              color: Colors.redAccent,
              child: Row(
                children: [
                  Icon(Icons.security),
                  SizedBox(child: Text('Enter your password')),
                ],
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Dashboard()));
                },
                child: Text('Login!',
                    style: TextStyle(fontSize: 32.0, color: Colors.blue)),
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => registration()));
                },
                child: Row(
                  children: [
                    Text('new user? '),
                    Text('register', style: TextStyle(color: Colors.blue)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class registration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Registration'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('Registration Page',
                style: TextStyle(fontSize: 32.0, color: Colors.blue)),
            Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.blueAccent),
                ),
                child:
                    TextField(decoration: InputDecoration(labelText: 'Name'))),
            Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.blueAccent),
                ),
                child:
                    TextField(decoration: InputDecoration(labelText: 'Phone'))),
            Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.blueAccent),
                ),
                child: TextField(
                    decoration: InputDecoration(labelText: 'Password'))),
            Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 2, color: Colors.blueAccent),
                ),
                child: TextField(
                    decoration:
                        InputDecoration(labelText: 'Confirm Password'))),
            Center(
              child: RaisedButton(
                onPressed: () {},
                child: Text('Submit',
                    style: TextStyle(fontSize: 32.0, color: Colors.blue)),
              ),
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Home'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
